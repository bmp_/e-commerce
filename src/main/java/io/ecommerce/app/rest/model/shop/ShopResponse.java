package io.ecommerce.app.rest.model.shop;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.ecommerce.app.model.Shop;
import io.ecommerce.app.rest.model.BaseResponse;
import org.springframework.http.HttpStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShopResponse extends BaseResponse {
    @JsonProperty("user")
    private Shop shop;

    public ShopResponse(Shop shop) {
        super(HttpStatus.OK);
        this.shop = shop;
    }

    public ShopResponse(HttpStatus status) {
        super(status);
    }
}
