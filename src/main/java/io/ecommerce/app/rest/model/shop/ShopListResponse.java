package io.ecommerce.app.rest.model.shop;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.ecommerce.app.model.Shop;
import io.ecommerce.app.model.User;
import io.ecommerce.app.rest.model.BaseResponse;
import org.springframework.http.HttpStatus;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShopListResponse extends BaseResponse {

    @JsonProperty("shops")
    private List<Shop> list;

    public ShopListResponse(List<Shop> list) {
        super(HttpStatus.OK);
        this.list = list;
    }

    public ShopListResponse(HttpStatus status) {
        super(status);
    }

    public List<Shop> getList() {
        return list;
    }

    public void setList(List<Shop> list) {
        this.list = list;
    }
}
