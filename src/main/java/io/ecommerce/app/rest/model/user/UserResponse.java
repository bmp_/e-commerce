package io.ecommerce.app.rest.model.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.ecommerce.app.model.User;
import io.ecommerce.app.rest.model.BaseResponse;
import org.springframework.http.HttpStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponse extends BaseResponse {

    @JsonProperty("user")
    private User user;

    public UserResponse(User user) {
        super(HttpStatus.OK);
        this.user = user;
    }

    public UserResponse(HttpStatus status) {
        super(status);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
