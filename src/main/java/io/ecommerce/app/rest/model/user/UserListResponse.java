package io.ecommerce.app.rest.model.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.ecommerce.app.model.User;
import io.ecommerce.app.rest.model.BaseResponse;
import org.springframework.http.HttpStatus;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserListResponse extends BaseResponse {

    @JsonProperty("users")
    private List<User> userList;

    public UserListResponse(List<User> list) {
        super(HttpStatus.OK);
        this.userList = list;
    }
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
