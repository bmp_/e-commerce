package io.ecommerce.app.rest.validator;

import io.ecommerce.app.util.constant.Constant;
import io.ecommerce.app.util.helper.StringHelper;

public class BaseValidator {

    public BaseValidator() {
        // Empty Constructor
    }

    public boolean notNull(Object obj) {
        return null != obj;
    }

    public boolean validate(String str) {
        return StringHelper.validate(str);
    }

    public boolean validate(String... strs) {
        for (String str : strs)
            if (!StringHelper.validate(str)) {
                return false;
            }
        return true;
    }
    public boolean validatePassword(String password) {
        return password.matches(Constant.PASSWORD_REGEX);

    }
}
