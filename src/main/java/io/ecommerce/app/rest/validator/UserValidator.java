package io.ecommerce.app.rest.validator;

import io.ecommerce.app.model.User;

import java.util.Map;

public class UserValidator extends BaseValidator{

    public UserValidator() {
        // Empty Constructor
    }

    public boolean validate(User user) {
        return notNull(user) && validate(user.getFullname()) && validate(user.getEmail())
                && validate(user.getPhone()) && validate(user.getPassword());
    }

    public boolean validate(Map<String, String> request) {
        return notNull(request) && request.containsKey("email") && validate(request.get("email"));
    }
}
