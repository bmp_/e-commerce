package io.ecommerce.app.rest.validator;

import io.ecommerce.app.rest.model.auth.AuthenticationRequest;

public class AuthenticationValidator extends BaseValidator{

    public AuthenticationValidator() {
        // Empty Constructor
    }

    public boolean validate(AuthenticationRequest request) {
        return notNull(request) && validate(request.getEmail()) && validate(request.getPassword());
    }
}
