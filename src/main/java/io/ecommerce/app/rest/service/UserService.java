package io.ecommerce.app.rest.service;

import io.ecommerce.app.manager.EncryptionManager;
import io.ecommerce.app.model.User;
import io.ecommerce.app.repository.UserRepository;
import io.ecommerce.app.rest.model.BaseResponse;
import io.ecommerce.app.rest.model.user.UserListResponse;
import io.ecommerce.app.rest.model.user.UserResponse;
import io.ecommerce.app.rest.validator.UserValidator;
import io.ecommerce.app.util.property.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(path = "users", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated
public class UserService extends BaseService {
    @Autowired
    private UserRepository userRepository;

    private UserValidator validator;

    public UserService() {
        log = getLogger(this.getClass());
        validator = new UserValidator();
    }

    @GetMapping()
    public UserListResponse getUserList() {
        final String methodName = "getUserList";
        start(methodName);
        List<User> userList = userRepository.getUserList();
        UserListResponse response = new UserListResponse(userList);
        completed(methodName);
        return response;
    }

    @RequestMapping(path = "details")
    public UserResponse getUserById(
            @RequestParam("userId") String userId) {
        final String methodName = "getUserList";
        start(methodName);
        UserResponse response = new UserResponse(HttpStatus.BAD_REQUEST);
        if (!userId.isEmpty()) {
            User user = userRepository.getUserDetail(userId);
            response = new UserResponse(user);
        }
        completed(methodName);
        return response;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserResponse createUser(@RequestBody User request) {
        final String methodName = "createUser";
        start(methodName);
        UserResponse response = new UserResponse(HttpStatus.BAD_REQUEST);
        if (validator.validate(request)) {

            boolean valid = userRepository.validateEmail(request.getEmail());
            if(!valid) {
                String userId = UUID.randomUUID().toString();
                request.setUid(userId);
                String salt = EncryptionManager.getInstance().generateRandomString(getIntProperty(Property.ENC_SALT_LENGTH));
                String encPassword = EncryptionManager.getInstance().hash(request.getPassword(), salt);

                request.setSalt(salt);
                request.setPassword(encPassword);

                boolean create = userRepository.createUser(request);
                if (create) {
                    User user = userRepository.getUserDetail(userId);
                    response = new UserResponse(user);
                    return response;
                }
            }
            else {
                response = new UserResponse(HttpStatus.CONFLICT);
            }
        }
        completed(methodName);
        return response;
    }

    @PostMapping(path = "email/validate", consumes = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse validateEmail(@RequestBody Map<String, String> requestMap) {
        final String methodName = "validateEmail";
        start(methodName);
        BaseResponse response = buildBadRequestResponse();
        if (validator.validate(requestMap)) {
            boolean valid = userRepository.validateEmail(requestMap.get("email"));
            response = valid? buildConflictResponse() : buildSuccessResponse();
        }
        completed(methodName);
        return response;
    }
}
