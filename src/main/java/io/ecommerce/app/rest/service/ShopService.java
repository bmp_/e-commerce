package io.ecommerce.app.rest.service;

import io.ecommerce.app.model.Shop;
import io.ecommerce.app.repository.ShopRepository;
import io.ecommerce.app.rest.model.shop.ShopListResponse;
import io.ecommerce.app.rest.model.shop.ShopResponse;
import io.ecommerce.app.rest.model.user.UserListResponse;
import io.ecommerce.app.rest.model.user.UserResponse;
import io.ecommerce.app.rest.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "shops", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated
public class ShopService extends BaseService {
    @Autowired
    private ShopRepository shopRepository;

    public ShopService() {
        log = getLogger(this.getClass());
    }

    @GetMapping()
    public ShopListResponse getAllShop() {
        final String methodName = "getAllShop";
        start(methodName);
        List<Shop> shops = shopRepository.getAllShop();
        ShopListResponse response = new ShopListResponse(shops);
        completed(methodName);
        return response;
    }

    @RequestMapping(path = "list")
    public ShopListResponse getUserShop(
            @RequestParam("userId") String userId) {
        final String methodName = "getUserShop";
        start(methodName);
        ShopListResponse response = new ShopListResponse(HttpStatus.BAD_REQUEST);
        if (!userId.isEmpty()) {
            List<Shop> shops = shopRepository.getUserShop(userId);
            response = new ShopListResponse(shops);
        }
        completed(methodName);
        return response;
    }

    @RequestMapping(path = "details")
    public ShopResponse getUserShopByName(
            @RequestParam("userId") String userId,
            @RequestParam("shopUrl") String shopUrl) {
        final String methodName = "getUserShopById";
        start(methodName);
        ShopResponse response = new ShopResponse(HttpStatus.BAD_REQUEST);
        Shop shop = shopRepository.getShopDetail(userId, shopUrl);
        response = new ShopResponse(shop);
        completed(methodName);
        return response;
    }


}
