package io.ecommerce.app.rest.service;

import io.ecommerce.app.manager.EncryptionManager;
import io.ecommerce.app.model.User;
import io.ecommerce.app.repository.UserRepository;
import io.ecommerce.app.rest.model.auth.AuthenticationRequest;
import io.ecommerce.app.rest.model.user.UserResponse;
import io.ecommerce.app.rest.validator.AuthenticationValidator;
import io.ecommerce.app.rest.validator.UserValidator;
import io.ecommerce.app.util.property.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "auth", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Validated
public class AuthenticationService extends BaseService {

    @Autowired
    private UserRepository userRepository;

    private AuthenticationValidator validator;

    public AuthenticationService() {
        log = getLogger(this.getClass());
        validator = new AuthenticationValidator();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserResponse authenticate(@RequestBody AuthenticationRequest request) {
        final String methodName = "authenticate";
        start(methodName);
        UserResponse response = new UserResponse(HttpStatus.BAD_REQUEST);
        if (validator.validate(request)) {

            boolean exist = userRepository.validateEmail(request.getEmail());
            if (exist) {
                String uid = userRepository.getUid(request.getEmail());
                String salt = userRepository.getSalt(uid);
                String encPassword = EncryptionManager.getInstance().hash(request.getPassword(), salt);

                boolean valid = userRepository.validateAuthenticate(request.getEmail(), encPassword);
                if (valid) {
                    User user = userRepository.getUserDetail(uid);
                    response = new UserResponse(user);
                } else {
                    response = new UserResponse(HttpStatus.UNAUTHORIZED);
                }
            } else {
                response = new UserResponse(HttpStatus.UNAUTHORIZED);
            }
        }
        completed(methodName);
        return response;
    }


}
