package io.ecommerce.app.rest.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.ecommerce.app.manager.PropertyManager;
import io.ecommerce.app.model.AuditLog;
import io.ecommerce.app.rest.model.BaseResponse;
import io.ecommerce.app.util.log.AppLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import io.ecommerce.app.util.json.JsonUtil;

public class BaseService {

    protected AppLogger log;

    public BaseService() {
        // Empty Constructor
    }

    protected AppLogger getLogger(Class<?> clazz) {
        return new AppLogger(clazz);
    }

    protected void start(String methodName) {
        log.debug("Start", methodName);
    }

    protected void completed(String methodName) {
        log.debug("Completed", methodName);
    }

    protected BaseResponse buildSuccessResponse() {
        return new BaseResponse(HttpStatus.OK);
    }

    protected BaseResponse buildSuccessResponse(Object obj) {
        return new BaseResponse(HttpStatus.OK);
    }

    protected BaseResponse buildBadRequestResponse() {
        return new BaseResponse(HttpStatus.BAD_REQUEST);
    }

    protected BaseResponse buildConflictResponse() {
        return new BaseResponse(HttpStatus.CONFLICT);
    }

    protected BaseResponse buildErrorResponse() {
        return new BaseResponse(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    protected String getProperty(String key) {
        return PropertyManager.getInstance().getProperty(key);
    }

    protected int getIntProperty(String key) {
        return Integer.parseInt(PropertyManager.getInstance().getProperty(key));
    }

    protected boolean getBooleanProperty(String key) {
        return PropertyManager.getInstance().getBooleanProperty(key);
    }

}
