package io.ecommerce.app;

import io.ecommerce.app.configuration.EncryptConfig;

public class Console {

/*    spring.datasource.dataSourceClassName=org.postgresql.ds.PGSimpleDataSource
    spring.datasource.dataSourceProperties.serverName=ENC(XkGMh7XeaOUGDKnmnJqVUfvxxG8Ohqt7)
    spring.datasource.dataSourceProperties.portNumber=ENC(S7wR5mFtblk+nV72fdpfrw==)
    spring.datasource.dataSourceProperties.databaseName=ENC(h1dlVGP7JVmuE95YjGoU+t0IrwoEQeOl)
    spring.datasource.username=ENC(AKLvFbUtGF2pZWPDRV/GU67ZN0jETxrf)
    spring.datasource.password=ENC(A24sK4dj107Kt4h1M1v/JDvc6E750NPV)*/

    public static void main(String[] args) {
        String[] strArr = {"XkGMh7XeaOUGDKnmnJqVUfvxxG8Ohqt7", "S7wR5mFtblk+nV72fdpfrw==", "h1dlVGP7JVmuE95YjGoU+t0IrwoEQeOl",
                "h1dlVGP7JVmuE95YjGoU+t0IrwoEQeOl", "AKLvFbUtGF2pZWPDRV/GU67ZN0jETxrf", "A24sK4dj107Kt4h1M1v/JDvc6E750NPV"};

        EncryptConfig config = new EncryptConfig();

        for (String str : strArr) {
            String encrypted = config.stringEncryptor().decrypt(str);
            System.out.println("Clear : " + str);
            System.out.println("Encrypted : ENC(" + encrypted + ")");
            System.out.println();
        }
    }
}
