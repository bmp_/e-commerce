package io.ecommerce.app.manager;

import io.ecommerce.app.util.constant.Constant;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

public class PropertyManager extends BaseManager {

	private static PropertyManager instance;

	private final Properties prop;

	private PropertyManager() {
		log = getLogger(this.getClass());
		start("Constructor");
		prop = new Properties();
		try {
			prop.load(PropertyManager.class.getClassLoader().getResourceAsStream(Constant.PROPERTY_FILENAME));

		} catch (Exception ex) {
			log.error("PropertyManager", "Error Loading Properties File", ex);
		}
		completed("Constructor");
	}

	private String getFileContent(String filename) {
		StringBuilder content = new StringBuilder();
		try (Scanner sc = new Scanner(this.getClass().getClassLoader().getResourceAsStream(filename))) {

			while (sc.hasNextLine()) {
				content.append(sc.nextLine());
			}
		} catch (Exception ex) {
			log.error("getFileContent", ex);
		}
		return content.toString();
	}

	public Set<Object> getKeySet() {
		return prop.keySet();
	}

	public String getProperty(String key) {
		return getProperty(key, "");
	}

	public int getIntProperty(String key) {
		return getIntProperty(key, 0);
	}

	public int getIntProperty(String key, int defaultValue) {
		return Integer.parseInt(getProperty(key, String.valueOf(defaultValue)));
	}

	public long getLongProperty(String key) {
		return getLongProperty(key, 0);
	}

	public long getLongProperty(String key, long defaultValue) {
		return Long.parseLong(getProperty(key, String.valueOf(defaultValue)));
	}

	public double getDoubleProperty(String key) {
		return getDoubleProperty(key, 0);
	}

	public double getDoubleProperty(String key, double defaultValue) {
		return Double.parseDouble(getProperty(key, String.valueOf(defaultValue)));
	}

	public boolean getBooleanProperty(String key) {
		return Boolean.parseBoolean((getProperty(key, "false")));
	}

	public Date getDateProperty(String key, String format) {
		DateFormat df = new SimpleDateFormat(format);
		Date retDate = new Date();
		try {
			retDate = df.parse(getProperty(key));
		} catch (ParseException e) {
			retDate = new Date(System.currentTimeMillis());
		}
		return retDate;
	}

	private String getProperty(String key, String defaultValue) {
		if (prop != null) {
			return prop.getProperty(key, defaultValue);
		}
		return defaultValue;
	}

	public static void shutdown() {
		if (instance != null) {
			instance.prop.clear();
		}
	}

	public static PropertyManager getInstance() {
		if (instance == null) {
			instance = new PropertyManager();
		}
		return instance;
	}
}
