package io.ecommerce.app.manager;

import io.ecommerce.app.util.json.JsonHelper;
import io.ecommerce.app.util.log.AppLogger;

public class BaseManager {

	protected AppLogger log;

	public BaseManager() {
		// Empty Constructor
	}

	protected AppLogger getLogger(Class<?> clazz) {
		return new AppLogger(clazz);
	}

	protected void start(String methodName) {
		log.info(methodName, "Start");
	}

	protected void completed(String methodName) {
		log.info(methodName, "Completed");
	}

	protected String toJson(Object obj) {
		return JsonHelper.toJson(obj);
	}
}
