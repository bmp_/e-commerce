package io.ecommerce.app;

import io.ecommerce.app.manager.PropertyManager;
import io.ecommerce.app.util.property.Property;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

        //System.out.println(PropertyManager.getInstance().getProperty(Property.CUSTOM_CONFIG_TEST));
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
