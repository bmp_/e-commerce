package io.ecommerce.app.util.property;

public class Property {

    private Property() {}

    public static final String ENC_SALT_LENGTH      = "enc.salt.lenth";
    public static final String AUDIT_ENABLED        ="audit.save.enable";
    public static final String CUSTOM_CONFIG_TEST        ="custom.config.test";
}
