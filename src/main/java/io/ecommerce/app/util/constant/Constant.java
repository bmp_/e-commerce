package io.ecommerce.app.util.constant;

public class Constant {

    private Constant() {}

    public static final String PROPERTY_FILENAME = "application.properties";

    public static final String PASSWORD_REGEX =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,30}$";
}
