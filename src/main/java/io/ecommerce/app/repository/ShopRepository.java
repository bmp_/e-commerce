package io.ecommerce.app.repository;

import io.ecommerce.app.model.Shop;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.statement.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ShopRepository extends BaseRepository {

    public ShopRepository() {
        log = getLogger(this.getClass());
    }

    public List<Shop> getAllShop() {
        final String methodName = "getAllShop";
        start(methodName);
        String sql = "SELECT uid, userid, \"shopName\", \"createDt\", \"modifyDt\", status, \"shopUrl\" FROM shop_basic;";
        List<Shop> result = new ArrayList<>();
        try (Handle h = getHandle(); Query query = h.createQuery(sql)) {
            result = query.mapToBean(Shop.class).list();
        } catch (Exception ex) {
            log.error(methodName, ex.getMessage());
        }

        completed(methodName);
        return result;
    }

    public List<Shop> getUserShop(String userId) {
        final String methodName = "getUserShop";
        start(methodName);
        String sql = "SELECT uid, userid, \"shopName\", \"createDt\", \"modifyDt\", status, \"shopUrl\" FROM shop_basic WHERE userid = :userId";
        List<Shop> result = new ArrayList<>();
        try (Handle h = getHandle(); Query query = h.createQuery(sql)) {
            query.bind("userId",userId);
            result = query.mapToBean(Shop.class).list();
        } catch (Exception ex) {
            log.error(methodName, ex.getMessage());
        }

        completed(methodName);
        return result;
    }

    public Shop getShopDetail(String userId, String shopUrl) {
        final String methodName = "getShopDetail";
        start(methodName);
        String sql = "SELECT uid, userid, \"shopName\", \"createDt\", \"modifyDt\", status, \"shopUrl\" FROM shop_basic WHERE userid = :userId AND \"shopUrl\" = :shopUrl;";
        Shop result = null;
        try (Handle h = getHandle(); Query query = h.createQuery(sql)) {
            query.bind("userId",userId);
            query.bind("shopUrl",shopUrl);
            result = query.mapToBean(Shop.class).one();
        } catch (Exception ex) {
            log.error(methodName, ex.getMessage());
        }

        completed(methodName);
        return result;
    }
}
