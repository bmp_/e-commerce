package io.ecommerce.app.repository;

import io.ecommerce.app.model.User;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.statement.Query;
import org.jdbi.v3.core.statement.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository extends BaseRepository {

    public UserRepository() {
        log = getLogger(this.getClass());
    }

    public List<User> getUserList() {
        final String methodName = "getUserList";
        start(methodName);

        String sql = "SELECT uid, fullname, email, phone, \"createDt\", \"modifyDt\", status FROM public.user;";

        List<User> result = new ArrayList<>();
        try (Handle h = getHandle(); Query query = h.createQuery(sql)) {
            result = query.mapToBean(User.class).list();
        } catch (Exception ex) {
            log.error(methodName, ex.getMessage());
        }

        completed(methodName);
        return result;
    }

    public User getUserDetail(String userId) {
        final String methodName = "getUserDetail";
        start(methodName);

        String sql = "SELECT uid, fullname, email, phone, \"createDt\", \"modifyDt\", status FROM public.user WHERE uid= :userId;";

        User result = null;
        try (Handle h = getHandle(); Query query = h.createQuery(sql)) {
            query.bind("userId", userId);
            result = query.mapToBean(User.class).findOnly();
        } catch (Exception ex) {
            log.error(methodName, ex.getMessage());
        }
        completed(methodName);
        return result;
    }

    public boolean createUser (User user)
    {
        final String methodName = "createUser";
        start(methodName);

        String sql = "INSERT INTO public.user (uid, fullname, email, phone, \"password\", salt) " +
                "VALUES(:uid, :fullname, :email, :phone, :password, :salt)";

        boolean result = false;
        try (Handle h = getHandle(); Update update = h.createUpdate(sql)) {
            update.bindBean(user);
            int row = update.execute();
            result = (row == 1);

        } catch (Exception ex) {
            log.error(methodName, ex.getMessage());
        }

        completed(methodName);
        return result;
    }

    public boolean validateEmail(String email) {
        final String methodName = "validateEmail";
        start(methodName);

        String sql = "select exists(select 1 from public.user where email = :email);";

        boolean result = false;
        try (Handle h = getHandle(); Query query = h.createQuery(sql)) {
            result  = query.bind("email", email).mapTo(boolean.class).one();

        } catch (Exception ex) {
            log.error(methodName, ex.getMessage());
        }
        completed(methodName);
        return result;
    }

    public String getUid(String email) {
        final String methodName = "getUid";
        start(methodName);

        String sql = "SELECT uid FROM public.user WHERE email = :email;";

        String result = null;
        try (Handle h = getHandle(); Query query = h.createQuery(sql)) {
            query.bind("email", email);
            result = query.mapTo(String.class).one();
        } catch (Exception ex) {
            log.error(methodName, ex.getMessage());
        }
        completed(methodName);
        return result;
    }

    public String getSalt(String uid) {
        final String methodName = "getSalt";
        start(methodName);

        String sql = "SELECT salt FROM public.user WHERE uid = :uid;";

        String result = null;
        try (Handle h = getHandle(); Query query = h.createQuery(sql)) {
            query.bind("uid", uid);
            result = query.mapTo(String.class).one();
        } catch (Exception ex) {
            log.error(methodName, ex.getMessage());
        }
        completed(methodName);
        return result;
    }

    public boolean validateAuthenticate(String email, String password) {
        final String methodName = "validateEmail";
        start(methodName);

        String sql = "select exists(select 1 from public.user where email = :email AND \"password\" = :password);";

        boolean result = false;
        try (Handle h = getHandle(); Query query = h.createQuery(sql)) {
            query.bind("email", email);
            query.bind("password", password);
            result  = query.mapTo(boolean.class).one();

        } catch (Exception ex) {
            log.error(methodName, ex.getMessage());
        }
        completed(methodName);
        return result;
    }
}
