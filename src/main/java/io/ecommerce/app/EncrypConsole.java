package io.ecommerce.app;

import io.ecommerce.app.manager.EncryptionManager;
import io.ecommerce.app.manager.PropertyManager;
import io.ecommerce.app.util.property.Property;

public class EncrypConsole {

    public static void main(String [] args)
    {
        String password = "P@ssw0rd";

        System.out.println("Pssword : "+ password);
        System.out.println("Enc Pssword : "+ EncryptionManager.getInstance().encrypt(password));

        String salt = EncryptionManager.getInstance().generateRandomString(PropertyManager.getInstance().getIntProperty(Property.ENC_SALT_LENGTH));
        System.out.println("Generated Salt : "+ salt );
        System.out.println("Hash Pssword : "+ EncryptionManager.getInstance().hash(password,salt));

    }
}
